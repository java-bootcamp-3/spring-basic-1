package com.basic;

import com.basic.configuration.MyCustomConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cglib.core.Local;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Locale;

@EnableScheduling
@EnableConfigurationProperties
@SpringBootApplication
public class SpringBasics1Application implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(SpringBasics1Application.class);

	@Autowired
	private MyCustomConfig customConfig;

	@Value("${com.app.name}")
	private String name;
	@Value("${com.app.description}")
	private String desc;

	@Autowired
	private MessageSource messageSource;

	public static void main(String[] args) {
		SpringApplication.run(SpringBasics1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		logger.info("Application Name {}, Description {}",name,desc);
		logger.info("Message default {}",messageSource.getMessage("greeting",null, Locale.getDefault()));
		logger.info("Message Fr {}",messageSource.getMessage("greeting",null, Locale.FRENCH));

	}
}
