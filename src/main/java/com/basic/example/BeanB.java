package com.basic.example;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

@ConditionalOnBean(BeanA.class)
@Component
public class BeanB {
}
