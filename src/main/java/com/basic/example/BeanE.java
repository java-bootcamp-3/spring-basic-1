package com.basic.example;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@ConditionalOnProperty(name = "com.beanE.enabled",havingValue = "true")
@Component
public class BeanE {
}
