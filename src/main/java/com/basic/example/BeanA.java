package com.basic.example;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class BeanA {

    @Scheduled(fixedRate = 5000)
    public void logTime(){
        System.err.println("Time now "+ LocalDateTime.now());
    }
}
