package com.basic.calculator.model;

import java.util.Arrays;

public enum OperatorEnum {

    ADDITION("+"),
    SUBSTRACT("-"),
    MULTIPLY("*"),
    DIVISION("%");

    private String value;

    OperatorEnum(String value){
        this.value = value;
    }

    public static OperatorEnum fromValue(String value){
        return Arrays.asList(OperatorEnum.values()).stream()
                .filter(e -> e.value.equals(value))
                .findFirst()
                .orElseThrow(()->new RuntimeException("Enum does not exist"));
    }



}
