package com.basic.calculator.service;

public interface Operator {
    Double calculate(Double...digits);
    Boolean isOperatorSupported(String operatorVal);
}
