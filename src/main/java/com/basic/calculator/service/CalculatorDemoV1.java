package com.basic.calculator.service;

import com.basic.SpringBasics1Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CalculatorDemoV1 implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(CalculatorDemoV1.class);


    @Autowired
    private CalculatorV1 calculatorV1;

    @Override
    public void run(String... args) throws Exception {
        logger.info("Addition V1: {} + {} = {}",5,5,calculatorV1.calculate("+",5.0,5.0));
        logger.info("Subtraction V1: {} - {} = {}",5,4,calculatorV1.calculate("-",5.0,4.0));
        logger.info("Multiplication V1: {} * {} = {}",5,5,calculatorV1.calculate("*",5.0,5.0));
        logger.info("Division V1: {} / {} = {}",10,2,calculatorV1.calculate("%",10.0,2.0));
    }
}
