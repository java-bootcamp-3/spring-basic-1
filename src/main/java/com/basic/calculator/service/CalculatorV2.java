package com.basic.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculatorV2 {

    @Autowired
    private List<Operator> operators;

    public Double calculate(String operator,Double... digits){
        return operators.stream()
                .filter(s -> s.isOperatorSupported(operator))
                .findFirst()
                .map(s -> s.calculate(digits))
                .orElseThrow(()-> new RuntimeException("Operator not supported"));
    }
}
