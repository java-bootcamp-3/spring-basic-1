package com.basic.calculator.service;

import com.basic.calculator.model.OperatorEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Qualifier("multiplyService")
@Service
public class MultiplyService implements Operator{

    private OperatorEnum operator = OperatorEnum.MULTIPLY;

    @Override
    public Double calculate(Double... digits) {
        return digits[0]*digits[1];
    }

    @Override
    public Boolean isOperatorSupported(String operatorVal) {
        return OperatorEnum.fromValue(operatorVal)==operator;
    }
}