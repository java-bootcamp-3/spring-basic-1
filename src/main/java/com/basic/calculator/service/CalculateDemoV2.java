package com.basic.calculator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class CalculateDemoV2 implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(CalculatorDemoV1.class);


    @Autowired
    private CalculatorV1 calculatorV2;

    @Override
    public void run(String... args) throws Exception {
        logger.info("Addition V2: {} + {} = {}",5,5,calculatorV2.calculate("+",5.0,5.0));
        logger.info("Subtraction V2: {} - {} = {}",5,4,calculatorV2.calculate("-",5.0,4.0));
        logger.info("Multiplication V2: {} * {} = {}",5,5,calculatorV2.calculate("*",5.0,5.0));
        logger.info("Division V2: {} / {} = {}",10,2,calculatorV2.calculate("%",10.0,2.0));
    }
}
