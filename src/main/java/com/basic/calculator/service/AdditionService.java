package com.basic.calculator.service;

import com.basic.calculator.model.OperatorEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Qualifier("additionService")
@Service
public class AdditionService implements Operator{

    private OperatorEnum operator = OperatorEnum.ADDITION;

    @Override
    public Double calculate(Double... digits) {
        return digits[0]+digits[1];
    }

    @Override
    public Boolean isOperatorSupported(String operatorVal) {
        return OperatorEnum.fromValue(operatorVal)==operator;
    }
}
