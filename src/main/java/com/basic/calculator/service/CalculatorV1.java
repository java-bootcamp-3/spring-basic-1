package com.basic.calculator.service;

import com.basic.calculator.model.OperatorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


import static com.basic.calculator.model.OperatorEnum.*;

@Service
public class CalculatorV1 {

    @Qualifier("additionService")
    @Autowired
    private Operator addition;

    @Qualifier("divisionService")
    @Autowired
    private Operator division;

    @Qualifier("multiplyService")
    @Autowired
    private Operator multiply;

    @Qualifier("substractService")
    @Autowired
    private Operator subtract;

    public Double calculate(String operator, Double... digits){
       return switch (OperatorEnum.fromValue(operator)){
            case ADDITION -> addition.calculate(digits);
            case DIVISION -> division.calculate(digits);
            case MULTIPLY -> multiply.calculate(digits);
            case SUBSTRACT -> subtract.calculate(digits);
            default -> 0.0;
        };
    }
}
